Format: 3.0 (quilt)
Source: slock-axdwm
Binary: slock-axdwm
Architecture: any
Version: 1.5+git20231006.316de88-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/slock-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev
Package-List:
 slock-axdwm deb x11 optional arch=any
Checksums-Sha1:
 6e4415ce9373ff7dabc4705160849261a6462986 173367 slock-axdwm_1.5+git20231006.316de88.orig.tar.gz
 3426ad046395730f67127744ee91cf6e8f5bde00 3516 slock-axdwm_1.5+git20231006.316de88-1.debian.tar.xz
Checksums-Sha256:
 bd97dc0bcd0675aa059b5322c70d42b96000d29aa53806ff4445719b1260562c 173367 slock-axdwm_1.5+git20231006.316de88.orig.tar.gz
 2c641417333be1e5357d9fa882ef4bc95fba33400e2c91f6d76f1456ab71abe1 3516 slock-axdwm_1.5+git20231006.316de88-1.debian.tar.xz
Files:
 ce4101ac460bfc83499406f3ce3d5069 173367 slock-axdwm_1.5+git20231006.316de88.orig.tar.gz
 b911f4f39e1ba2597b193c952ddae5a3 3516 slock-axdwm_1.5+git20231006.316de88-1.debian.tar.xz
