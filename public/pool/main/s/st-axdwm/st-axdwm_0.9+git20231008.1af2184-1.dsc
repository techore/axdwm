Format: 3.0 (quilt)
Source: st-axdwm
Binary: st-axdwm
Architecture: any
Version: 0.9+git20231008.1af2184-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/st-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev
Package-List:
 st-axdwm deb x11 optional arch=any
Checksums-Sha1:
 0cc2abb14eb3b9913ac6daaaed8ff10d9cb9eac2 498017 st-axdwm_0.9+git20231008.1af2184.orig.tar.gz
 0c25def500b45368ffd4f5289426f75e5a6207f6 3584 st-axdwm_0.9+git20231008.1af2184-1.debian.tar.xz
Checksums-Sha256:
 c91076d5f61497cb5a0b18e8262cd29f841bf6398ae73ef594c6550e2b68fb27 498017 st-axdwm_0.9+git20231008.1af2184.orig.tar.gz
 4b72d70e5288e6f240ac565c76f2b3c3ab132b2f9b85056b47100b57fb32a8e6 3584 st-axdwm_0.9+git20231008.1af2184-1.debian.tar.xz
Files:
 00045b2c04ee255f210b9febbd96c6e7 498017 st-axdwm_0.9+git20231008.1af2184.orig.tar.gz
 a1dab1913554d1f8c96650f05f7447c8 3584 st-axdwm_0.9+git20231008.1af2184-1.debian.tar.xz
