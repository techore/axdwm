Format: 3.0 (quilt)
Source: slstatus-axdwm
Binary: slstatus-axdwm
Architecture: any
Version: 0.0+git20230428.fb334a1-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/slstatus-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev
Package-List:
 slstatus-axdwm deb x11 optional arch=any
Checksums-Sha1:
 c563b55e232778300c7d4af209d106b9be7b4b34 537123 slstatus-axdwm_0.0+git20230428.fb334a1.orig.tar.gz
 39e60940186fbf46fa3dc813fb5f8c74df1fac7a 5180 slstatus-axdwm_0.0+git20230428.fb334a1-1.debian.tar.xz
Checksums-Sha256:
 f530ae7e38bb495ef0ad68841f46d83781266465d24e8306647cedb792ea5941 537123 slstatus-axdwm_0.0+git20230428.fb334a1.orig.tar.gz
 01c2dbc9fddb22f9576bb20239c840e3b0d24fc39112a6c856872f648cb50326 5180 slstatus-axdwm_0.0+git20230428.fb334a1-1.debian.tar.xz
Files:
 42488a4ed1ac1276d9b2fca0efc184a1 537123 slstatus-axdwm_0.0+git20230428.fb334a1.orig.tar.gz
 d79a022e8fddce8f3737186f06756c14 5180 slstatus-axdwm_0.0+git20230428.fb334a1-1.debian.tar.xz
