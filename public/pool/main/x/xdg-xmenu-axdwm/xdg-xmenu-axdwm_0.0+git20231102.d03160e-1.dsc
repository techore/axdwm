Format: 3.0 (quilt)
Source: xdg-xmenu-axdwm
Binary: xdg-xmenu-axdwm
Architecture: any
Version: 0.0+git20231102.d03160e-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/xdg-xmenu-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev, libinih-dev
Package-List:
 xdg-xmenu-axdwm deb x11 optional arch=any
Checksums-Sha1:
 f3b946488e06c53aaa8d40b8b948d39ed84714cb 201353 xdg-xmenu-axdwm_0.0+git20231102.d03160e.orig.tar.gz
 dc04d8bd41604a2776a17cfc2c824849da85cf44 2416 xdg-xmenu-axdwm_0.0+git20231102.d03160e-1.debian.tar.xz
Checksums-Sha256:
 4e9974e8ee027ed1840d23fcf526d04ef6a8e115e7d5e4c2a99b01586d2f5728 201353 xdg-xmenu-axdwm_0.0+git20231102.d03160e.orig.tar.gz
 62c17dfb60ce6fcf2afb587ad9adc7160dc605e58c7027d8eb5d0685010727f9 2416 xdg-xmenu-axdwm_0.0+git20231102.d03160e-1.debian.tar.xz
Files:
 d5567eaefd24b7cfa83cdf8a9b0c926d 201353 xdg-xmenu-axdwm_0.0+git20231102.d03160e.orig.tar.gz
 1c89bd2d202ee350c20f76d2fc0bdec5 2416 xdg-xmenu-axdwm_0.0+git20231102.d03160e-1.debian.tar.xz
