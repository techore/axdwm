Format: 3.0 (quilt)
Source: xmenu-axdwm
Binary: xmenu-axdwm
Architecture: any
Version: 0.0+git20230903.8c4013a-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/xmenu-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev
Package-List:
 xmenu-axdwm deb x11 optional arch=any
Checksums-Sha1:
 d54a03edaf6d0247a48a27795e776296a4dc04fe 118600 xmenu-axdwm_0.0+git20230903.8c4013a.orig.tar.gz
 db2c209fbd1cc24c44907f9647ad3732588a69fd 2836 xmenu-axdwm_0.0+git20230903.8c4013a-1.debian.tar.xz
Checksums-Sha256:
 152be62cd25932e54b2825992bf51e21ee41fe2571e23951b7acf201355febbb 118600 xmenu-axdwm_0.0+git20230903.8c4013a.orig.tar.gz
 64055cdb2c48068a0cfbbaf73022e95fe4aee6a366ce7e7cc2e31f0376d81065 2836 xmenu-axdwm_0.0+git20230903.8c4013a-1.debian.tar.xz
Files:
 b32153f3d39facfd1112437efc8b94b0 118600 xmenu-axdwm_0.0+git20230903.8c4013a.orig.tar.gz
 3db334e8890ce0440903b0337346b858 2836 xmenu-axdwm_0.0+git20230903.8c4013a-1.debian.tar.xz
