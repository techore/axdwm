Format: 3.0 (quilt)
Source: dmenu-axdwm
Binary: dmenu-axdwm
Architecture: any
Version: 0.0+git20230922.7534a2d-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/dmenu-axdwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev, libxinerama-dev
Package-List:
 dmenu-axdwm deb x11 optional arch=any
Checksums-Sha1:
 823200b501810034ecb30fa124398eb36ca6963c 92425 dmenu-axdwm_0.0+git20230922.7534a2d.orig.tar.gz
 76407798a198c3ecbf9433ea59ae37bf37ef9afa 3176 dmenu-axdwm_0.0+git20230922.7534a2d-1.debian.tar.xz
Checksums-Sha256:
 b79fcc54dc322fda6a780ded62144d09cb4b86cd2133c61710e9607384f3f467 92425 dmenu-axdwm_0.0+git20230922.7534a2d.orig.tar.gz
 db5bd25181a7b8b5ca44861ac4970b6bcdb9e3bf12978034f120219e1ae1a498 3176 dmenu-axdwm_0.0+git20230922.7534a2d-1.debian.tar.xz
Files:
 c36cb219f1e1072e739c386ee0fd656e 92425 dmenu-axdwm_0.0+git20230922.7534a2d.orig.tar.gz
 cb843b789da0469dc76a587a3e1bfe60 3176 dmenu-axdwm_0.0+git20230922.7534a2d-1.debian.tar.xz
