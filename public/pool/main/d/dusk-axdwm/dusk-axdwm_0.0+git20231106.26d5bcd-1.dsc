Format: 3.0 (quilt)
Source: dusk-axdwm
Binary: dusk-axdwm
Architecture: any
Version: 0.0+git20231106.26d5bcd-1
Maintainer: techore <axdwm@pm.me>
Homepage: https://gitlab.com/techore/dusk
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13), libx11-dev, libx11-xcb-dev, libxcb-res0-dev, libxft-dev, libimlib2-dev, libyajl-dev, libxinerama-dev, libxrandr-dev, libxfixes-dev, libxi-dev, libfribidi-dev
Package-List:
 dusk-axdwm deb x11 optional arch=any
Checksums-Sha1:
 64cca1116838a178ca8c427e123256d7e434eebf 1502902 dusk-axdwm_0.0+git20231106.26d5bcd.orig.tar.gz
 fe53e8854a225fa42442000ba8fa49e1877dd51d 41864 dusk-axdwm_0.0+git20231106.26d5bcd-1.debian.tar.xz
Checksums-Sha256:
 bc41e6fbf8b2979bd3ce6cb91d0476ff2b0d5825af91b71611da02a662d3b319 1502902 dusk-axdwm_0.0+git20231106.26d5bcd.orig.tar.gz
 1eed39ddfdb9f4ad99a1c312bd008f0faeb6e21c5156db1266bd1294b115b476 41864 dusk-axdwm_0.0+git20231106.26d5bcd-1.debian.tar.xz
Files:
 026240b38454104d5f443a4d7a670547 1502902 dusk-axdwm_0.0+git20231106.26d5bcd.orig.tar.gz
 e62e4da7190a5e83f4d1eb52b7ea2e2e 41864 dusk-axdwm_0.0+git20231106.26d5bcd-1.debian.tar.xz
